package com.pizza.pizzaorderingservices.Controller;

import com.pizza.pizzaorderingservices.Entity.Customer;
import com.pizza.pizzaorderingservices.Entity.Pizza;
import com.pizza.pizzaorderingservices.Services.PizzaService;
import com.pizza.pizzaorderingservices.UTIL.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PizzaController {
    @Autowired
    private PizzaService pizzaService;

    @PostMapping("/pizza")
    public ResponseEntity<Object> createPizza(@RequestBody Pizza pizza) {
        Pizza createdPizza = pizzaService.createPizza(pizza);

        if (createdPizza != null) {
            return ResponseUtil.createResponse("Successfully created pizza", createdPizza, true);
        } else {
            return ResponseUtil.createErrorResponse("Failed to create pizza.", false, "pizza name is must", 500);
        }


    }

    @GetMapping("/pizzas")
    public ResponseEntity<List<Pizza>> getAllPizzas() {
        List<Pizza> pizzas = pizzaService.getAllPizzas();
        return new ResponseEntity<>(pizzas, HttpStatus.OK);
    }

    @GetMapping("/piz/{pizzaId}")
    public ResponseEntity<Pizza> getPizzaById(@PathVariable Long pizzaId) {
        Pizza pizza = pizzaService.getPizzaById(pizzaId);
        return ResponseEntity.ok(pizza);

    }

    @PutMapping("/pizza/{pizzaId}")
    public Pizza updatePizza(@PathVariable Long pizzaId, @RequestBody Pizza pizza) {
        pizza.setPizzaId(pizzaId);
        return pizzaService.updatePizza(pizza);
    }
    @DeleteMapping("/pizzaDELETE/{pizzaId}")
    public ResponseEntity<String> deletePizza(@PathVariable Long pizzaId) {
        try {
            pizzaService.deletePizza(pizzaId);
            return ResponseEntity.ok("PIZZA deleted successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to delete pizza: " + e.getMessage());
        }
    }
}
