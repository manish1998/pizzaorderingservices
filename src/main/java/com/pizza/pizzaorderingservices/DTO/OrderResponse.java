package com.pizza.pizzaorderingservices.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pizza.pizzaorderingservices.Entity.Orders;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderResponse {
    Boolean success;
    String message;
    Orders orders;
    Object Data;

    public OrderResponse(boolean b, String successfullyFetchedOrders, List<Orders> orderList) {
    }
}
