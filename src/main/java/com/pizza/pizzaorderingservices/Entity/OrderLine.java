package com.pizza.pizzaorderingservices.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderLine {
    private Long orderLineId;
    //  private Long OrderId;
    private Long orderId;
    private Long pizzaId;
    private String Size;
   private Integer quantity;
    private double totalPrice;
  // private Orders order;
   // private Pizza pizza;

}
