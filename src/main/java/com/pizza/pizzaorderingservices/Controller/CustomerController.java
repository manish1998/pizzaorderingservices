package com.pizza.pizzaorderingservices.Controller;

import com.pizza.pizzaorderingservices.Entity.Customer;
import com.pizza.pizzaorderingservices.Services.CustomerService;
import com.pizza.pizzaorderingservices.UTIL.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Slf4j
@RestController
public class CustomerController {
    @Autowired
    private CustomerService customerService;


    @PostMapping("/customer")
    public ResponseEntity<Object> createCustomer(@RequestBody Customer customer) {
              customerService.createCustomer(customer);
        log.info("CUSTOMER CREATED SUCCESSFULLY");
            return ResponseEntity.status(HttpStatus.CREATED).body("Customer created successfully.");

    }

    @GetMapping("/customers")
    public ResponseEntity<List<Customer>> getAllCustomers() {
        List<Customer> customers = customerService.getAllCustomers();
        log.info("fetched all customer");
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }
    @GetMapping("/cust/{customerId}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable Long customerId) {
        Customer customer = customerService.getCustomerById(customerId);
        log.info("fetch customer by ID");
        return ResponseEntity.ok(customer);
    }
    @PutMapping("/customer/{customerId}")
    public Customer updateCustomer(@PathVariable Long customerId, @RequestBody Customer customer) {
        customer.setCustomerId(customerId);
        log.info("EDITED customer successfully");
        return customerService.updateCustomer(customer);
    }
    @DeleteMapping("/Delete/{customerId}")
    public ResponseEntity<String> deleteCustomer(@PathVariable Long customerId) {
        try {
            customerService.deleteCustomer(customerId);
            log.info("deleted successfully");
            return ResponseEntity.ok("Customer deleted successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to delete customer: " + e.getMessage());

        }
    }
}



