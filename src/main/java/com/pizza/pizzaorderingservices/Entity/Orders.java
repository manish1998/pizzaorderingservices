package com.pizza.pizzaorderingservices.Entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Orders {
    private Long customerId;
    private Long orderId;
    private String Status;
   private double totalAmount;
    private String orderDateTime;
    private String deliveryAddress;
  // private Customer customer;
 //  private List<OrderLine>orderLine;
    private List<Pizza>pizza;

}
