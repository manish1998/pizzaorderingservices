package com.pizza.pizzaorderingservices.Mapper;

import com.pizza.pizzaorderingservices.Entity.Customer;
import com.pizza.pizzaorderingservices.Entity.Pizza;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface PizzaMapper {
    void createPizza(@Param("pizza") Pizza pizza);
    List<Pizza> getAllPizzas();
    Pizza getPizzaById(Long pizzaId);
    void updatePizza(Pizza pizza);
    void deletePizza(Long pizzaId);
}
