package com.pizza.pizzaorderingservices.Controller;


import com.pizza.pizzaorderingservices.DTO.OrderResponse;
import com.pizza.pizzaorderingservices.Entity.Orders;
import com.pizza.pizzaorderingservices.Entity.Pizza;
import com.pizza.pizzaorderingservices.Services.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class OrdersController {
    @Autowired
    private OrdersService ordersService;

    @PostMapping("/orders")
    public ResponseEntity<OrderResponse> createOrder(@RequestBody Orders orders) {
        OrderResponse orderResponse = ordersService.createOrder(orders);
        return new ResponseEntity<>(orderResponse, HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity<OrderResponse> getAllOrders() {
        OrderResponse orderResponse = ordersService.getAllOrders();
        return new ResponseEntity<>(orderResponse, HttpStatus.OK);
    }



    

}





