package com.pizza.pizzaorderingservices.Services;


import com.pizza.pizzaorderingservices.DTO.OrderResponse;
import com.pizza.pizzaorderingservices.Entity.Orders;

import java.util.List;

public interface OrdersService {


       public OrderResponse createOrder(Orders orders);

       OrderResponse getAllOrders();

       //  List<OrderResponse> getOrders();
}
