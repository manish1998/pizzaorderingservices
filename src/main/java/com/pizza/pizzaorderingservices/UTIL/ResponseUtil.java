package com.pizza.pizzaorderingservices.UTIL;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;


    public class ResponseUtil {

        public static ResponseEntity<Object> createResponse(String message, Object data, boolean success) {
            Map<String, Object> response = new HashMap<>();
            response.put("Success",true);
            response.put("Message", message);
            response.put("Data", data);
            return ResponseEntity.ok(response);

        }
        public static ResponseEntity<Object> createErrorResponse(String message,boolean success,String errorMessage,int errorCode) {
            Map<String, Object> error = new HashMap<>();
            error.put("Code", errorCode);
            error.put("Message", errorMessage);


            Map<String, Object> response = new HashMap<>();
            response.put("Success", false);
            response.put("Message", message);
            response.put("Error", error);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }


