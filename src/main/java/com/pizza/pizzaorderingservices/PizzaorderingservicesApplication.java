package com.pizza.pizzaorderingservices;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan("com.pizza.pizzaorderingservices.Mapper")
@SpringBootApplication
public class PizzaorderingservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaorderingservicesApplication.class, args);
	}

}
