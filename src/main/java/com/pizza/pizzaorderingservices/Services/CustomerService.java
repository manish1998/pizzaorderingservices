package com.pizza.pizzaorderingservices.Services;

import com.pizza.pizzaorderingservices.Entity.Customer;
import com.pizza.pizzaorderingservices.Entity.Pizza;

import java.util.List;

public interface CustomerService {
   public void createCustomer(Customer customer);

   List<Customer> getAllCustomers();

   Customer getCustomerById(Long customerId);

   Customer updateCustomer(Customer customer);

   void deleteCustomer(Long customerId);

}
