package com.pizza.pizzaorderingservices.Services;

import com.pizza.pizzaorderingservices.Entity.Customer;
import com.pizza.pizzaorderingservices.Mapper.CustomerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl  implements  CustomerService {
    @Autowired
    private CustomerMapper customerMapper;

    @Override
    public void createCustomer(Customer customer) {

        customerMapper.insertCustomer(customer);


    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerMapper.getAllCustomers();
    }

    @Override
    public Customer getCustomerById(Long customerId) {
        return customerMapper.getCustomerById(customerId);
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        customerMapper.updateCustomer(customer);
        return customerMapper.getCustomerById(customer.getCustomerId());
    }

    @Override
    public void deleteCustomer(Long customerId) {
        customerMapper.deleteCustomer(customerId);
    }
}

