package com.pizza.pizzaorderingservices.Services;

import com.pizza.pizzaorderingservices.Entity.Pizza;
import com.pizza.pizzaorderingservices.Mapper.PizzaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PizzaServiceImpl implements PizzaService {
    @Autowired
    private PizzaMapper pizzaMapper;

    @Override
    public Pizza createPizza(Pizza pizza) {
        pizzaMapper.createPizza(pizza);
        return pizza;
    }

    @Override
    public List<Pizza> getAllPizzas() {
        return pizzaMapper.getAllPizzas();
    }

    @Override
    public Pizza getPizzaById(Long pizzaId) {
        return pizzaMapper.getPizzaById(pizzaId);
    }

    @Override
    public Pizza updatePizza(Pizza pizza) {
        pizzaMapper.updatePizza(pizza);
        return pizzaMapper.getPizzaById(pizza.getPizzaId());
    }

    @Override
    public void deletePizza(Long pizzaId) {
        pizzaMapper.deletePizza(pizzaId);
    }
}




