package com.pizza.pizzaorderingservices.Exception;

public class PizzaNotFoundException extends  RuntimeException{
    public PizzaNotFoundException(String message) {
        super(message);
    }
}
