package com.pizza.pizzaorderingservices.Entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Pizza {



    private Integer quantity;
    private String Size;
    private Long pizzaId;
    private String Name;
    private String Description;
    private String Type;
    private String Image_url;
    private double priceRegularSize;
    private double priceMediumSize;

    private double priceLargeSize;

    private List<OrderLine> orderLine;
}
