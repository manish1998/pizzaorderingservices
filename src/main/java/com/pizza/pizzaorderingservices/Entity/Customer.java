package com.pizza.pizzaorderingservices.Entity;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Customer {

    @NotBlank(message = "mandatory field")
    private Long customerId;
    @NotBlank(message = "first is must")
    private String firstName;
    @NotBlank(message = "lastname is must")
    private String lastName;
    @NotBlank(message = "address field is mandatory")
    private String Address;
    @NotBlank(message = "phone number is mandatory")
    private String Phone;
    @NotBlank(message = "email is mandatory")
    private String email;

    private List<Orders> order;
}
