package com.pizza.pizzaorderingservices.Services;

import com.pizza.pizzaorderingservices.DTO.OrderResponse;
import com.pizza.pizzaorderingservices.Entity.OrderLine;
import com.pizza.pizzaorderingservices.Entity.Orders;
import com.pizza.pizzaorderingservices.Entity.Pizza;
import com.pizza.pizzaorderingservices.Mapper.OrderLineMapper;
import com.pizza.pizzaorderingservices.Mapper.OrdersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrdersService {
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private OrderLineMapper orderLineMapper;

    @Override
    public OrderResponse createOrder(Orders orders) {
        System.out.println("inside create Order");

        // Save Order
        Orders createdOrder = Orders.builder().orderDateTime(String.valueOf(new Date())).customerId(orders.getCustomerId()).Status("Pending").totalAmount(500).deliveryAddress(orders.getDeliveryAddress()).orderId(orders.getOrderId()).build();

        // order set rem fileds
        ordersMapper.createOrder(createdOrder);

        Long orderId = createdOrder.getOrderId();

        // Iterate over the list of pizzas in the order
        for (Pizza pizza : orders.getPizza()) {
            // Create an OrderLine entity for each pizza

            OrderLine orderLine = new OrderLine();
            orderLine.setOrderId(orderId);
            orderLine.setPizzaId(pizza.getPizzaId());
            orderLine.setQuantity(pizza.getQuantity());
            orderLine.setSize(pizza.getSize());


            //  orderLine.setTotalPrice(orders.getTotalAmount());
            orderLine.setTotalPrice(pizza.getPriceRegularSize() * pizza.getQuantity());

            // set sub total
            // Save the OrderLine
            orderLineMapper.createOrderLine(orderLine);
        }

        OrderResponse orderResponse = OrderResponse.builder().success(true).message("Order Created SuccesFully").orders(createdOrder).Data(orders.getPizza()).build();

        return orderResponse;

    }
    @Override
    public OrderResponse getAllOrders() {
        List<Orders> ordersList = ordersMapper.getAllOrders();
        List<Orders> orderList = new ArrayList<>();

        for (Orders orders : ordersList) {
            Orders order = new Orders();
            order.setOrderId(orders.getOrderId());
            order.setCustomerId(orders.getCustomerId());
            order.setDeliveryAddress(orders.getDeliveryAddress());
            order.setStatus(orders.getStatus());
            order.setTotalAmount(orders.getTotalAmount());
            order.setOrderDateTime(orders.getOrderDateTime());

            List<OrderLine> orderLines = orderLineMapper.getOrderLinesByOrderId(orders.getOrderId());
            List<Pizza> pizzas = new ArrayList<>();

            for (OrderLine orderLine : orderLines) {
                Pizza pizza = new Pizza();
                pizza.setPizzaId(orderLine.getPizzaId());
                pizza.setSize(orderLine.getSize());
                pizza.setQuantity(orderLine.getQuantity());
               // pizza.sets(orderLine.getTotalPrice());
                pizzas.add(pizza);
            }

            order.setPizza(pizzas);
            orderList.add(order);
        }

        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setSuccess(true);
        orderResponse.setMessage("Data found");
        orderResponse.setData(orderList);

        return orderResponse;
    }
}







