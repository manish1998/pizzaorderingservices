package com.pizza.pizzaorderingservices.Services;

import com.pizza.pizzaorderingservices.Entity.Pizza;

import java.util.List;

public interface PizzaService {
    Pizza createPizza(Pizza pizza);


    List<Pizza> getAllPizzas();

    Pizza getPizzaById(Long pizzaId);

    Pizza updatePizza(Pizza pizza);

    void deletePizza(Long pizzaId);
}
