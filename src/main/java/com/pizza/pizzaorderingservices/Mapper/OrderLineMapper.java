package com.pizza.pizzaorderingservices.Mapper;

import com.pizza.pizzaorderingservices.Entity.OrderLine;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderLineMapper {
    void createOrderLine(@Param("orderLine") OrderLine orderLine);
    List<OrderLine> getOrderLinesByOrderId(Long orderId);


}
