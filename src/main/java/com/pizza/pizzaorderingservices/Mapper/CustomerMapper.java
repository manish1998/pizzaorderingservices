package com.pizza.pizzaorderingservices.Mapper;

import com.pizza.pizzaorderingservices.Entity.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Optional;

@Mapper
public interface CustomerMapper {
    void insertCustomer(@Param("customer") Customer customer);
    List<Customer> getAllCustomers();
    Customer getCustomerById(Long customerId);
    void updateCustomer(Customer customer);
    void deleteCustomer(Long customerId);
}
