package com.pizza.pizzaorderingservices.Mapper;

import com.pizza.pizzaorderingservices.DTO.OrderResponse;
import com.pizza.pizzaorderingservices.Entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrdersMapper {
     Long createOrder(@Param("order") Orders order);
  //   List<OrderResponse> getOrdersWithPizza();
  List<Orders> getAllOrders();

}
